<?php
    require ('bootstrap.php');
    require_once ('includes/layout/header.php');

    $error = [];
    $success = false;

    $fields = [
        'title' => '',
        'text' => ''
    ];

    if(isset($_POST['save'])) {

        $fields['title'] = trim($_POST['title']);
        $fields['text'] = trim($_POST['text']);

        if(!strlen($fields['title'])) {
            $error[] = 'Bitte Titel eingeben';
        }

        if(!strlen($fields['text'])) {
            $error[] = 'Bitte Text eingeben';
        }

        if(!count($error)) {

            $query = "
                insert into notes
                  (title, text)
                values
                  ('".$fields['title']."', '".$fields['text']."')
            ";

            try {
                $db->exec($query);
                $success = true;
            } catch(Exception $e) {
                $error[] = 'Eintrag konnte nicht gespeichert werden!';
            }
        }
    }
?>

<h3>Anlegen</h3>

<? if(count($error)): ?>
    <div class="error message">
        <ul>
        <? foreach ($error as $message): ?>
            <li><?= $message ?></li>
        <? endforeach ?>
        </ul>
    </div>
<? endif ?>

<? if($success): ?>
    <div class="success message">
        Der Eintrag wurde angelegt!<br/>
        <a href="/">Zur Liste</a>
    </div>
<? endif ?>


<form method="post">



    <div class="form-field">
        <label for="name">Titel</label>
        <input type="text" name="title"  value="<?= $fields["title"] ?>">
    </div>

    <div class="form-field">
        <label for="text">Text</label>
        <textarea rows="5" cols="40" name="text"><?= $fields["text"] ?></textarea>
    </div>

    <div class="form_action">
        <input type="submit" name="save" value="Anlegen" />
    </div>

</form>
