<?php
    require ('bootstrap.php');
    require_once ('includes/layout/header.php');

    $error = [];

    if(isset($_GET['id'])) {
        try {
            $stmt = $dbh->query('DELETE FROM notes WHERE id='.$_GET['id']);
            $fields = $stmt->execute();
        } catch (Exception $e) {
            $error[] = 'Eintrag konnte nicht gelöscht werden';
        }
    }
    else {
        $error[] = "Dieser Eintrag existiert nicht";
    }

?>

<h3>Löschen</h3>

<? if(count($error)): ?>
    <div class="error message">
        <ul>
        <? foreach ($error as $message): ?>
            <li><?= $message ?></li>
        <? endforeach ?>
        </ul>
    </div>
<? else: ?>
    <div class="success message">
        Der Eintrag wurde gelöscht!<br/>
        <a href="/">Zur Liste</a>
    </div>
<? endif ?>

