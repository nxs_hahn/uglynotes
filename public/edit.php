<?php
    require ('bootstrap.php');
    require_once ('includes/layout/header.php');

    $error = [];
    $success = false;
    $loaded = false;

    $fields = [
        'id' => '',
        'title' => '',
        'text' => ''
    ];

    if (isset($_POST['id'])) {
        $fields['title'] = trim($_POST['title']);
        $fields['text'] = trim($_POST['text']);
        $fields['id'] = trim($_POST['id']);

        if(!strlen($fields['title'])) {
            $error[] = 'Bitte Titel eingeben';
        }

        if(!strlen($fields['text'])) {
            $error[] = 'Bitte Text eingeben';
        }

        if(!count($error)) {

            $query = "
                UPDATE notes
                SET
                  title = '".$fields['title']."',
                  TEXT = '".$fields['text']."'
                WHERE
                    id = ".$fields['id'];
            ;

            try {
                $db->exec($query);
                $success = true;
            } catch(Exception $e) {
                $error[] = 'Eintrag konnte nicht gespeichert werden!';
            }
        }
        $loaded = true;
    } elseif(isset($_GET['id'])) {
        try {
            $stmt = $dbh->query('SELECT * FROM notes WHERE id='.$_GET['id']);
            $fields = $stmt->fetch(PDO::FETCH_ASSOC);
            $loaded = true;
        } catch (Exception $e) {
            $error[] = 'Eintrag konnte nicht geladen werden';
        }
    }
    else {
        $error[] = "Dieser Eintrag existiert nicht";
    }

?>

<h3>Bearbeiten</h3>

<? if(count($error)): ?>
    <div class="error message">
        <ul>
        <? foreach ($error as $message): ?>
            <li><?= $message ?></li>
        <? endforeach ?>
        </ul>
    </div>
<? endif ?>

<? if($loaded): ?>


    <? if($success): ?>
        <div class="success message">
            Der Eintrag wurde geändert!<br/>
            <a href="/">Zur Liste</a>
        </div>
    <? endif ?>


    <form method="post">

        <input type="hidden" name="id" value="<?= $fields['id'] ?>">

        <div class="form-field">
            <label for="name">Titel</label>
            <input type="text" name="title"  value="<?= $fields["title"] ?>">
        </div>

        <div class="form-field">
            <label for="text">Text</label>
            <textarea rows="5" cols="40" name="text"><?= $fields["text"] ?></textarea>
        </div>

        <div class="form_action">
            <input type="submit" name="save" value="Anlegen" />
        </div>

    </form>

<? endif ?>
