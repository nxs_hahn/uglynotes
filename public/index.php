<?php
require ('bootstrap.php');

$rows = $dbh->query('SELECT * FROM notes ORDER BY created');

require('includes/layout/header.php');

?>


<?php foreach ($rows as $row): ?>
    <div class="note">
        <h3><?= $row['title'] ?></h3>
        <p>
            <?= $row['text'] ?>
        </p>
        <div class="action">
            <a href="edit.php?id=<?= $row['id'] ?>">Edit</a>
            <a href="delete.php?id=<?= $row['id'] ?>">Löschen</a>
        </div>
    </div>
<?php endforeach ?>

<?php
include('includes/layout/footer.php');
?>
